
function is_touch_device() {
  return !!('ontouchstart' in window);
}

jQuery(window).load(function () {

    $('.doc-loader').fadeOut('slow');

    displayHints();

    /******************************************************************************/
    /*  SLIDERS                                                                   */
    /******************************************************************************/
    $('#testimonials-slides').carouFredSel({
        responsive: true,
        auto: false,
        align: 'center',
        height: 'variable',
        scroll: 1,
        prev: '#testimonials-prev',
        next: '#testimonials-next',
        swipe: {
            onMouse: true,
            onTouch: true
        }, items: {
            height: 'variable'
        }
    });

    $('#home-slides').carouFredSel({
        responsive: true,
        auto: {
            play: true,
            pauseOnHover: true,
            pauseOnResize: true,
            duration: 500,
            timeoutDuration: 3000
        },
        align: 'center',
        height: 'auto',
        scroll: 1,
        next: {
            button: "#home-next",
            easing: "easeOutCirc",
            duration: 800
        },
        swipe: {
            onMouse: true,
            onTouch: true
        }, items: {
            height: 'variable'
        },
        items: {
            start: "random"
        }
    });

    $('#bios-slides').carouFredSel({
        responsive: true,
        auto: false,
        align: 'center',
        height: 'variable',
        scroll: 1,
        prev: '#bios-prev',
        next: '#bios-next',
        swipe: {
            onMouse: false,
            onTouch: false
        }, items: {
            height: 'variable'
        }
    });

    $('#news-slides').carouFredSel({
        responsive: true,
        auto: false,
        align: 'center',
        height: 'variable',
        scroll: 1,
        prev: '#news-prev',
        next: '#news-next',
        swipe: {
            onMouse: false,
            onTouch: false
        }, items: {
            height: 'variable'
        }
    });

    /******************************************************************************/
    /*   BUTTON HOVER                                                             */
    /******************************************************************************/
    if (!(is_touch_device())) {
        jQuery('#testimonials-prev, #testimonials-next, #home-next, #news-prev, #news-next, .read-more, #bios-prev, #bios-next').hover(function () {
            jQuery(this).css('background-position', 'left bottom');
        }, function () {
            jQuery(this).css('background-position', 'left top');
        });
    }
    else {
        jQuery('#testimonials-prev, #testimonials-next, #home-next, #news-prev, #news-next, .read-more, #bios-prev, #bios-next').bind('touchstart touchend', function (e) {
            jQuery(this).toggleClass('hover_effect');
        });
    }
    if (!(is_touch_device())) {
        jQuery('.submit-btn, #subscribeButton').hover(function () {
            jQuery(this).css('background-color', '#20607d');
        }, function () {
            jQuery(this).css('background-color', '#2a7ca2');
        });
    }
    else {
        jQuery('.submit-btn, #subscribeButton').bind('touchstart, touchend', function (e) {
            jQuery(this).toggleClass('hover_effect');
        });
    }
    if (!(is_touch_device())) {
        jQuery('.news h2').hover(function () {
            jQuery(this).css('color', '#5d7728');
        }, function () {
            jQuery(this).css('color', '#c0db89');
        });
        jQuery('.services h2').hover(function () {
            jQuery(this).css('color', '#5d7728');
        }, function () {
            jQuery(this).css('color', '#89BB22');
        });
        jQuery('.about h2.read-more').hover(function () {
            jQuery(this).css('color', '#5d7728');
        }, function () {
            jQuery(this).css('color', '#89BB22');
        });
    }

    /******************************************************************************/
    /*  PORTFOLIO ITEM IMAGE HOVER                                                */
    /******************************************************************************/
    jQuery("ul.gallery li .item-overlay, ul.gallery-bios li .item-overlay").hide();

    if (is_touch_device()) {
        jQuery("ul.gallery li, ul.gallery-bios li").click(function () {

            var items_before = jQuery(this).prevAll("li").length;
            var opacity = jQuery(this).find(".item-overlay").css("opacity");
            var display = jQuery(this).find(".item-overlay").css("display");

            if ((opacity == 0) || (display == "none")) {
                jQuery(this).find(".item-overlay").fadeTo(300, 0.9);
            } else {
                jQuery(this).find(".item-overlay").fadeTo(300, 0);
            }

            jQuery(this).parent().find("li:lt(" + items_before + ") .item-overlay").fadeTo(300, 0);
            jQuery(this).parent().find("li:gt(" + items_before + ") .item-overlay").fadeTo(300, 0);
        });

    }
    else {
        jQuery("ul.gallery li, ul.gallery-bios li").hover(function () {
            jQuery(this).find(".item-overlay").fadeTo(250, 0.9);
        }, function () {
            jQuery(this).find(".item-overlay").fadeTo(250, 0);
        });

    }
});

/******************************************************************************/
/*  MAKE IT RETINA                                                               */
/******************************************************************************/
$.makeItRetina({
    'retinaSuffix': '@2x',
    'consoleDebugMessages': true
});

/******************************************************************************/
/*  HELPER METHODS                                                            */
/******************************************************************************/
var displayHints = function()
{
	if(jQuery().attachHint) {		
		jQuery('#subscriberEmail').attachHint('Enter your email to subscribe:');
	}
}
var ResetInput = function(){
    jQuery('input').each(function() {
        jQuery(this).val('').text('');
    });	
};